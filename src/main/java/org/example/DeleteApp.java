package org.example;

import org.example.entity.Song;
import org.example.utils.HibernateUtils;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;


public class DeleteApp
{

    public static void main(String[] args) {

        Session session = HibernateUtils.getSessionFactory().openSession();

        // load object we want to delete
        Song song = session.get(Song.class, 1);

        session.beginTransaction();
        // delete object
        session.delete(song);

        session.getTransaction().commit();
    }

}
