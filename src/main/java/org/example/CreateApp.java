package org.example;

import org.example.entity.Song;
import org.example.utils.HibernateUtils;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;


public class CreateApp
{

    public static void main(String[] args) {

        // initialize session object
        Session session = HibernateUtils.getSessionFactory().openSession();

        Song song1 = new Song();

        song1.setId(2);
        song1.setSongName("Le chant des patriotes");
        song1.setArtist("PCF");

        Song song2 = new Song(4, "Manif", "Orelsan");

        // initialize transaction to be able to save data to db (spring?)
        session.beginTransaction();

        session.save(song2);

        // commit changes to db
        session.getTransaction().commit();

        System.out.println("song saved... check db...");

        session.close();
    }

}
