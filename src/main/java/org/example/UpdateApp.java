package org.example;

import org.example.entity.Song;
import org.example.utils.HibernateUtils;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;


public class UpdateApp
{

    public static void main(String[] args) {

        SessionFactory sessionFactory = HibernateUtils.getSessionFactory();

        if(sessionFactory != null) {

            Session session = sessionFactory.openSession();

            Song song = session.get(Song.class, 4);

            // song.setSongName("ALLELUYA");

            // session.beginTransaction();
            // session.update(song);
            // session.getTransaction().commit();

            session.beginTransaction();
            // here we don't call session.update() we directly set the songName after initializing transaction and VOILA
            song.setSongName("Manif");

            session.getTransaction().commit();
        }

    }

}
