package org.example;

import org.example.entity.Song;
import org.example.utils.HibernateUtils;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import javax.persistence.Query;


public class ReadApp
{

    public static void main(String[] args) {

        Session session = HibernateUtils.getSessionFactory().openSession();

        // ORM/JPA query
        Song song = session.get(Song.class, 2);

        // Song song = session.load(Song.class, 2);

        // print orm/jpa result
        System.out.println("------------------------------------------------------------------------------------");
        System.out.println("--------------------------------ORM/JPA QUERY RESULT----------------------------------");
        System.out.println("song object : " + song);
        System.out.println("song id : " + song.getId());
        System.out.println("song name : " + song.getSongName());
        System.out.println("song artist : " + song.getArtist());

        // hql query
        Query q = session.createQuery("from Song as song1 WHERE id=:id")
                .setParameter("id", 3);

        Song song1 = (Song) q.getSingleResult();

        // print hql result
        System.out.println("------------------------------------------------------------------------------------");
        System.out.println("--------------------------------HQL QUERY RESULT----------------------------------");
        System.out.println("song object : " + song1);
        System.out.println("song id : " + song1.getId());
        System.out.println("song name : " + song1.getSongName());
        System.out.println("song artist : " + song1.getArtist());


        // hql query
        Query nbr = session.createQuery("select COUNT(*) from Song");

        // print hql result
        System.out.println("------------------------------------------------------------------------------------");
        System.out.println("--------------------------------HQL QUERY RESULT----------------------------------");
        System.out.println("Nombre de chansons dans la base de donnée : " + nbr.getSingleResult());


    }

}
