package org.example.utils;

import org.example.entity.Song;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateUtils {

    private static SessionFactory sessionFactory = null;

    public static SessionFactory getSessionFactory() {

        try {

            // we make sure that only one sessionFactory will be initialized
            if(sessionFactory == null) {
                // create configuration
                Configuration configuration = new Configuration();
                configuration.configure("hibernate.cfg.xml");
                configuration.addAnnotatedClass(Song.class);

                //create session factory (=datasource)
                sessionFactory = configuration.buildSessionFactory(); // immutable
            }

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Session Factory object not created");
        }

        return sessionFactory;
    }
}
